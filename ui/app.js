/******************************************************************************
 * 
 * Imports
 * 
 *****************************************************************************/ 

// React
const React = require('react');
const ReactDOM = require('react-dom');

// Redux
const {Provider} = require('react-redux');
const {createStore} = require('redux');

const AppReducer = require('./model/AppReducer');

// Views
const LayoutViewFactory = require('./views/LayoutView');

require("react-loose-forms.bootstrap3").install(require("react-loose-forms/InputTypes"));

/**************************************
 * Components
 *************************************/
 
let store = createStore(AppReducer);

const LayoutView = LayoutViewFactory();

/**************************************
 * Render
 *************************************/

ReactDOM.render(<Provider store={store}><LayoutView/></Provider>, document.getElementById('react-target'));