/******************************************************************************
 *
 * Imports
 *
 *****************************************************************************/

// NPM
const React = require('react');
const {createClass, PropTypes} = React;
const {connect} = require('react-redux');
const {is} = require('immutable');
const _ = require('lodash');

// Enums
const {MODES} = require('../enums');

// Actions
const {ContactActionsFactory, NavActionsFactory} = require('../actions');

// Components
const ContactEditFormFactory = require('../forms/ContactEditForm');

/**
 * Generates a ContactEdit View
 *
 * @param {object} spec - Container for named parameters to modify private member behavior
 * @returns {*} - React Component
 * @constructor
 */
function ContactEditViewFactory(spec) {
    /**************************************************************************
     *
     * Private Members
     *
     *************************************************************************/

    /**********************************
     * Actions
     *********************************/

    const {saveContact} = ContactActionsFactory();
    const {goToContactList} = NavActionsFactory();

    /**********************************
     * Methods
     *********************************/

    let _cancelHandler;
    let _mapStateToProps;
    let _submitHandler;

    /**
     *
     * @param inst
     * @private
     */
    _cancelHandler = (inst) => {
        const {dispatch} = inst.props;

        dispatch(goToContactList());
    };

    /**
     * Takes in the application state and extracts the properties from it that it
     * would like to have passed in as props.  It will be added as a listener
     * for these items when they are changed.
     *
     * @param {object} state
     * @returns {object}
     * @private
     */
    _mapStateToProps = (state) => {
        return {
            nav: state.nav
        };
    };

    /**
     * Handles the successfully validated form submission
     *
     * @param {object} form
     * @param {object} inst
     * @private
     */
    _submitHandler = (form, inst) => {
        const {nav, dispatch} = inst.props;
        const contact = nav.get('selectedContact');
        const mode = nav.get('mode');

        let contactObject;

        if(contact && mode === MODES.EDIT) {
            contactObject = contact.toJS();
        }

        // form is flattened and needs to be re-organized
        contactObject = _.assign(contactObject, _.pick(form, [
            'firstName',
            'lastName',
            'workPhone',
            'homePhone',
            'mobilePhone'
        ]));

        contactObject['address'] = _.pick(form, [
            'street',
            'street2',
            'city',
            'state',
            'zip'
        ]);

        saveContact(contactObject, dispatch);
    };

    /**********************************
     * Components
     *********************************/

    const ContactEditForm = ContactEditFormFactory();

    /**************************************************************************
     *
     * Public Interface / React Component
     *
     *************************************************************************/

    let component = createClass({
        /**
         * Used in debug messages
         */
        displayName: 'ContactEditView',
        /**
         * Specifies what we expect in 'this.props'. Allows you to validate
         * props being passed to your components.
         */
        propTypes: {
            dispatch: PropTypes.func.isRequired,
            nav: PropTypes.object.isRequired
        },
        /**
         * Invoked before rendering when new props or state are being received.
         * Use this as an opportunity to return false when you're certain
         * that the transition to the new props and state will not require
         * a component update.
         *
         * @param nextProps
         * @param nextState
         */
        shouldComponentUpdate(nextProps, nextState) {
            if(!is(this.props.nav, nextProps.nav)) {
                return true;
            }

            return false;
        },
        /**
         * The render() method is required. Generates the virtual DOM/HTML.
         * @returns {*}
         */
        render() {
            const {nav} = this.props;
            const selectedContact = nav.get('selectedContact');
            const mode = nav.get('mode');

            return <div className="">
                <ContactEditForm contact={selectedContact}
                                 mode={mode}
                                 onSubmit={(form) => {
                                    _submitHandler(form, this);
                                 }}
                                 onCancel={(event) => {
                                     event.preventDefault();

                                     _cancelHandler(this);
                                 }}
                />
            </div>;
        }
    });

    // Connects the component to the store and the dispatcher
    return connect(_mapStateToProps)(component);
}

module.exports = ContactEditViewFactory;