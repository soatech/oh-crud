/******************************************************************************
 * 
 * Imports
 * 
 *****************************************************************************/

// NPM
const React = require('react');
const {createClass, PropTypes} = React;

// Components
const {FooterFactory, HeaderFactory} = require('../components');

// Views
const NavWrapperFactory = require('./NavWrapper');

/**
 * Generates a LayoutView Component
 * 
 * @param {object} spec - Container for named parameters to change private member behavior
 * @returns {*} - React Component
 * @constructor
 */
function LayoutViewFactory(spec) {
    /**************************************************************************
     * 
     * Private Members
     * 
     *************************************************************************/
     
    /**********************************
     * Components
     *********************************/
    
    const Footer = FooterFactory(); 
    const Header = HeaderFactory();
    
    /**********************************
     * Views
     *********************************/
     
    const NavWrapper = NavWrapperFactory();
    
    /**************************************************************************
     * 
     * Publice Interface / React Component
     * 
     *************************************************************************/
     
    return createClass({
        /**
         * Used in debug messages
         */
        displayName: 'LayoutView',
        /**
         * The render() method is required. Generates the virtual DOM/HTML.
         * @returns {*}
         */
        render() {
            return <div className="layoutContainer">
                <Header/>
                <NavWrapper/>
                <Footer/>
            </div>;
        }
    });
}

module.exports = LayoutViewFactory;