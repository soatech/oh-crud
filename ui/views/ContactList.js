/******************************************************************************
 *
 * Imports
 *
 *****************************************************************************/

// NPM
const React = require('react');
const {createClass, PropTypes} = React;
const {connect} = require('react-redux');
const {is} = require('immutable');

// Enums
const {VIEWS} = require('../enums');

// Components
const {ContactListItemFactory} = require('../components');

// Actions
const {ContactActionsFactory, NavActionsFactory} = require('../actions');

/**
 * Generates a ContactList View
 *
 * @param {object} spec - Container for named parameters to modify private member behavior
 * @returns {*} - React Component
 * @constructor
 */
function ContactListFactory(spec) {
    /**************************************************************************
     *
     * Private Members
     *
     *************************************************************************/

    /**********************************
     * Actions
     *********************************/

    const {loadContacts} = ContactActionsFactory();
    const {goToContactAdd} = NavActionsFactory();

    /**********************************
     * Methods
     *********************************/

    let _addClickHandler;
    let _mapStateToProps;

    /**
     *
     * @param inst
     * @private
     */
    _addClickHandler = (inst) => {
        const {dispatch} = inst.props;

        dispatch(goToContactAdd());
    };

    /**
     * Takes in the application state and extracts the properties from it that it
     * would like to have passed in as props.  It will be added as a listener
     * for these items when they are changed.
     *
     * @param {object} state
     * @returns {object}
     * @private
     */
    _mapStateToProps = (state) => {
        return {
            contacts: state.contacts
        };
    };

    /**********************************
     * Components
     *********************************/

    const ContactListItem = ContactListItemFactory();

    /**************************************************************************
     *
     * Public Interface / React Component
     *
     *************************************************************************/

    let component = createClass({
        /**
         * Used in debug messages
         */
        displayName: 'ContactList',
        /**
         * Specifies what we expect in 'this.props'. Allows you to validate
         * props being passed to your components.
         */
        propTypes: {
            dispatch: PropTypes.func.isRequired,
            contacts: PropTypes.object.isRequired
        },
        /**
         * Invoked before rendering when new props or state are being received.
         * Use this as an opportunity to return false when you're certain
         * that the transition to the new props and state will not require
         * a component update.
         *
         * @param nextProps
         * @param nextState
         */
        shouldComponentUpdate(nextProps, nextState) {
            if (!is(this.props.contacts, nextProps.contacts)) {
                return true;
            }

            return false;
        },
        /**
         * Invoked once immediately after the initial rendering occurs.
         */
        componentDidMount() {
            const {contacts, dispatch} = this.props;

            if (!contacts || !contacts.size) {
                loadContacts(dispatch);
            }
        },
        /**
         * The render() method is required. Generates the virtual DOM/HTML.
         * @returns {*}
         */
        render() {
            const {contacts} = this.props;

            return <div className="">
                {contacts.map((contact) => {
                    return <ContactListItem key={contact.get('id')} contact={contact}/>;
                })}
                <button className='btn btn-primary' onClick={(event) => {
                    event.preventDefault();

                    _addClickHandler(this);
                }}>Add Contact
                </button>
            </div>;
        }
    });

    // Connects the component to the store and the dispatcher
    return connect(_mapStateToProps)(component);
}

module.exports = ContactListFactory;