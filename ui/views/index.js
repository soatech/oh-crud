module.exports = {
    ContactEditViewFactory: require('./ContactEditView'),
    ContactListFactory: require('./ContactList')
};