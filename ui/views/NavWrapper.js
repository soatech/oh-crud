/******************************************************************************
 * 
 * Imports
 * 
 *****************************************************************************/
 
// NPM
const React = require('react');
const {createClass, PropTypes} = React;
const {connect} = require('react-redux');
const {is} = require('immutable');

// Enums
const {VIEWS} = require('../enums');

// Views
const {ContactEditViewFactory, ContactListFactory} = require('./');

/**
 * Generates a NavWrapper View
 * 
 * @param {object} spec - Container for named parameters to modify private member behavior
 * @returns {*} - React Component
 * @constructor
 */
function NavWrapperFactory(spec) {
    /**************************************************************************
     * 
     * Private Members
     *
     *************************************************************************/
    
    /**********************************
     * Views
     *********************************/

    const ContactEditView = ContactEditViewFactory();
    const ContactList = ContactListFactory();
     
    /**********************************
     * Methods
     *********************************/
     
    let _determineView;
    let _mapStateToProps;
    
    /**
     * Determines the view to be rendered
     * 
     * @param {object} inst - Reference to the public interface/react component
     * @returns {*} - React component to be rendered
     * @private
     */
    _determineView = (inst) => {
        const {nav} = inst.props;
        const view = nav.get('view');
        
        switch(view) {
            case VIEWS.CONTACT_EDIT:
                return <ContactEditView/>;
            case VIEWS.CONTACT_LIST:
            default:
                return <ContactList/>;
        }
    };
    
    /**
     * Takes in the application state and extracts the properties from it that it
     * would like to have passed in as props.  It will be added as a listener
     * for these items when they are changed.
     * 
     * @param {object} state
     * @returns {object}
     * @private
     */
    _mapStateToProps = (state) => {
        return {
            nav: state.nav
        };
    };
     
    /**************************************************************************
     * 
     * Public Interface / React Component
     *
     *************************************************************************/
     
    let component = createClass({
        /**
         * Used in debug messages
         */
        displayName: 'NavWrapper',
        /**
         * Specifies what we expect in 'this.props'. Allows you to validate
         * props being passed to your components.
         */
        propTypes: {
            dispatch: PropTypes.func.isRequired,
            nav: PropTypes.object.isRequired
        },
         /**
         * Invoked before rendering when new props or state are being received.
         * Use this as an opportunity to return false when you're certain
         * that the transition to the new props and state will not require
         * a component update.
         *
         * @param nextProps
         * @param nextState
         */
        shouldComponentUpdate(nextProps, nextState) {
            if(!is(this.props.nav, nextProps.nav)) {
                return true;
            }
            
            return false;
        },
        /**
         * The render() method is required. Generates the virtual DOM/HTML.
         * @returns {*}
         */
        render() {
            return <div className="container">
                {_determineView(this)}
            </div>;
        }
    });
    
    // Connects the component to the store and the dispatcher
    return connect(_mapStateToProps)(component);
}

module.exports = NavWrapperFactory;