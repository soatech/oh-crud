module.exports = {
    ACTIONS: require('./ACTIONS'),
    MODES: require('./MODES'),
    VIEWS: require('./VIEWS')
};