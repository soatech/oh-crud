/******************************************************************************
 * 
 * Imports
 * 
 *****************************************************************************/
 
const {fromJS, List} = require('immutable');

const {ACTIONS} = require('../enums');

/**
 * Generates a ContactReducer
 * @param {object} spec - Container for named parameters to override private 
 * member behavior
 * @returns {function} - Handler/switchboard for this reducer's actions
 * @constructor
 */
function ContactReducerFactory(spec) {
    
    /**************************************************************************
     * 
     * Private Members
     * 
     *************************************************************************/
    
    let _addContact;
    let _deleteContact;
    let _loadContacts;
    let _updateContact;
    
    /**
     * Adds a contact to the collection
     * 
     * @param {List<Record>|List<ContactRecord>} state
     * @param {object} action
     * @property {string} action.type
     * @property {object} action.contact
     * @returns {List<Record>|List<ContactRecord>}
     */
    _addContact = (state, action) => {
        return state.push(fromJS(action.contact));
    };
    
    /**
     * Removes a contact from the collection
     * 
     * @param {List<Record>|List<ContactRecord>} state
     * @param {object} action
     * @property {string} action.type
     * @property {Record|ContactRecord} action.contact
     * @returns {List<Record>|List<ContactRecord>}
     */
    _deleteContact = (state, action) => {
        // returns a new collection that has all entries except the one provided
        return state.filter((contact) => {
            return (contact.get('id') !== action.contact.get('id'));
        });
    };

    /**
     * Sets the contacts collection
     *
     * @param {List<Record>|List<ContactRecord>} state
     * @param {object} action
     * @property {string} action.type
     * @property {Array} action.contacts
     * @returns {List<Record>|List<ContactRecord>}
     */
    _loadContacts = (state, action) => {
        return fromJS(action.contacts);
    };
    
    /**
     * Updates a contact in the collection
     * 
     * @param {List<Record>|List<ContactRecord>} state
     * @param {object} action
     * @property {string} action.type
     * @property {object} action.contact
     * @returns {List<Record>|List<ContactRecord>}
     */
    _updateContact = (state, action) => {
        let foundIndex = undefined;

        state.map((contact, i) => {
            if(contact.get('id') === action.contact.id) {
                foundIndex = i;
            }
            
            return contact;
        });

        if(foundIndex !== undefined) {
            const updateModel = state.get(foundIndex);
            return state.set(foundIndex, updateModel.merge(fromJS(action.contact)));
        }

        return state;
    };
    
    /**************************************************************************
     * 
     * Public Interface
     * 
     *************************************************************************/
     
    /**
     * Handler for all actions this Reducer cares about
     *
     * @param {null|List<Record>|List<ContactRecord>} state
     * @param {object} action
     * 
     * @returns {List<Record>|List<ContactRecord>}
     */
    return (state, action) => {
        if(!state) {
            state = List();
        }
         
        switch (action.type) {
            case ACTIONS.CONTACT_ADD:
                return _addContact(state, action);
            case ACTIONS.CONTACT_DELETE:
                return _deleteContact(state, action);
            case ACTIONS.CONTACTS_LOAD:
                return _loadContacts(state, action);
            case ACTIONS.CONTACT_UPDATE:
                return _updateContact(state, action);
            default:
                return state;
        }
    };
}

module.exports = ContactReducerFactory;