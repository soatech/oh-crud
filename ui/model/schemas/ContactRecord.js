/******************************************************************************
 * 
 * Imports
 * 
 *****************************************************************************/
 
const {Record} = require('immutable');

const AddressModel = require('./AddressModel');

/******************************************************************************
 * 
 * Exports
 * 
 *****************************************************************************/

module.exports = Record({
    id: undefined,
    firstName: undefined,
    lastName: undefined,
    workPhone: undefined,
    homePhone: undefined,
    mobilePhone: undefined,
    address: new AddressModel()
});