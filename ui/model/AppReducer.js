/******************************************************************************
 * 
 * Imports
 * 
 *****************************************************************************/

// NPM
const {combineReducers} = require('redux');

// Reducers
const ContactReducerFactory = require('./ContactReducer');
const NavReducerFactory = require('./NavReducer');

/******************************************************************************
 * 
 * Private Members
 * 
 *****************************************************************************/

/**************************************
 * Components
 *************************************/

const contacts = ContactReducerFactory();
const nav = NavReducerFactory();

const AppReducer = combineReducers({
    contacts,
    nav
});

/******************************************************************************
 * 
 * Exports/Public Interface
 * 
 *****************************************************************************/
 
module.exports = AppReducer;