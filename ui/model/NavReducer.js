/******************************************************************************
 * 
 * Imports
 * 
 *****************************************************************************/
 
const {fromJS} = require('immutable');
const NavRecord = require('./schemas/NavRecord');

const {ACTIONS} = require('../enums');

/**
 * Generates a NavReducer
 * @param {object} spec - Container for named parameters to override private 
 * member behavior
 * @returns {function} - Handler/switchboard for this reducer's actions
 * @constructor
 */
function NavReducerFactory(spec) {
    
    /**************************************************************************
     * 
     * Private Members
     * 
     *************************************************************************/
    
    let _updateNav;
    
    /**
     * Updates the nav store
     * 
     * @param {Record|NavRecord} state
     * @param {object} action
     * @property {string} action.type
     * @property {Record|NavRecord} action.payload
     * @returns {Record|NavRecord}
     */
    _updateNav = (state, action) => {
        return state.merge(fromJS(action.payload));
    };
    
    /**************************************************************************
     * 
     * Public Interface
     * 
     *************************************************************************/
     
    /**
     * Handler for all actions this Reducer cares about
     *
     * @param {null|List<Record>|List<ContactRecord>} state
     * @param {object} action
     * 
     * @returns {List<Record>|List<ContactRecord>}
     */
    return (state, action) => {
        if(!state) {
            state = new NavRecord();
        }
         
        switch (action.type) {
            case ACTIONS.NAV_UPDATE:
                return _updateNav(state, action);
            default:
                return state;
        }
    };
}

module.exports = NavReducerFactory;