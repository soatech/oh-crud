/******************************************************************************
 *
 * Imports
 *
 *****************************************************************************/

// NPM
const React = require('react');
const {createClass, PropTypes} = React;
const _ = require('lodash');

// Mixins
const FormMixin = require('react-loose-forms');
const v = require('react-loose-forms.validation');

const VerticalFields = require('react-loose-forms.bootstrap3/layouts/VerticalFields');
const FormButtons = require('react-loose-forms.bootstrap3/layouts/FormButtons');

// Enums
const {MODES} = require('../enums');

/**
 * Generates a ContactEdit Form
 *
 * @param {object} spec
 * @returns {*} - React Component
 * @mixes FormMixin
 * @constructor
 */
function ContactEditFormFactory(spec) {
    /**************************************************************************
     *
     * Private Members
     *
     *************************************************************************/

    /**********************************
     * Methods
     *********************************/

    let _buildStateOptions;

    /**
     *
     * @returns {object}
     * @private
     */
    _buildStateOptions = () => {
        let state = {

        };

        return state;
    };

    /**************************************************************************
     *
     * Public Interface / React Component
     *
     *************************************************************************/

    return createClass({
        /**
         * Used in debug messages
         */
        displayName: 'ContactEditForm',
        /**
         * Specifies what we expect in 'this.props'. Allows you to validate
         * props being passed to your components.
         */
        propTypes: {
            contact: PropTypes.object,
            mode: PropTypes.string.isRequired,
            onSubmit: PropTypes.func.isRequired,
            onCancel: PropTypes.func.isRequired
        },
        /**
         * Allows you to use mixins to share behavior among multiple components.
         */
        mixins: [FormMixin],

        buildSchema() {
            return {
                firstName: {
                    type: 'text',
                    label: 'First Name',
                    validate: v.required
                },
                lastName: {
                    type: 'text',
                    label: 'Last Name',
                    validate: v.required
                },
                workPhone: {
                    type: 'text',
                    label: 'Work Phone',
                    placeholder: '(123) 456-7890',
                    validate: v.blankOr(v.phone)
                },
                homePhone: {
                    type: 'text',
                    label: 'Work Phone',
                    placeholder: '(123) 456-7890',
                    validate: v.blankOr(v.phone)
                },
                mobilePhone: {
                    type: 'text',
                    label: 'Work Phone',
                    placeholder: '(123) 456-7890',
                    validate: v.blankOr(v.phone)
                },
                street: {
                    type: 'text',
                    label: 'Street'
                },
                street2: {
                    ype: 'text',
                    label: ''
                },
                city: {
                    type: 'text',
                    label: 'City'
                },
                state: {
                    type: 'text',
                    label: 'State',
                    validate: v.blankOr((value) => {
                        let validFormat = /^[A-Za-z]{2}$/i;
                        return (validFormat.test(value) || "Enter a state in a valid format. E.g. NY, UT, etc.");
                    })
                },
                zip: {
                    type: 'text',
                    label: 'Zip',
                    validate: v.blankOr(v.integer)
                }
            };
        },

        getInitialValues(props) {
            const {contact, mode} = props;

            let initVals = {};

            if(contact && mode === MODES.EDIT) {
                const contactObj = contact.toJS();

                initVals = _.assign({}, _.omit(contactObj, ['address']), contactObj.address);
            }

            return initVals;
        },

        render() {
            const {onCancel} = this.props;
            const {errors} = this.state;

            return <form onSubmit={this.Form_onSubmit}>
                <VerticalFields
                    fields={this.Form_buildSchema()}
                    errors={errors || {}}
                    buildInput={this.Form_buildInput} />

                <FormButtons
                    right_align="true"
                    submit_btn_text="Save"
                    onDiscard={(event) => {
                        event.preventDefault();

                        this.Form_reset();
                    }}
                    onCancel={onCancel}
                />
            </form>;
        }
    });
}

module.exports = ContactEditFormFactory;