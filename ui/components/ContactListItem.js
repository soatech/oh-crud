/******************************************************************************
 *
 * Imports
 *
 *****************************************************************************/

// NPM
const React = require('react');
const {createClass, PropTypes} = React;
const {connect} = require('react-redux');
const {is} = require('immutable');

// Components
const ContactDetailsFactory = require('./ContactDetails');
const ContactItemActionsFactory = require('./ContactItemActions');
const ContactDeleteConfirmFactory = require('./ContactDeleteConfirm');

// Actions
const {ContactActionsFactory, NavActionsFactory} = require('../actions');

// Enums
const {MODES, VIEWS} = require('../enums');

/**
 * Generates a ContactListItem Component
 *
 * @param {object} spec - Container for named parameters to modify private member behavior
 * @returns {*} - React Component
 * @constructor
 */
function ContactListItemFactory(spec) {
    /**************************************************************************
     *
     * Private Members
     *
     *************************************************************************/

    /**********************************
     * Actions
     *********************************/

    const {deleteContact} = ContactActionsFactory();
    const {goToContactEdit} = NavActionsFactory();

    /**********************************
     * Methods
     *********************************/

    let _deleteHandler;
    let _editHandler;
    let _toggleDeleteConfirm;
    let _toggleDetails;

    /**
     * Handles the Delete confirmation
     *
     * @param {object} inst - Reference to the react component
     * @private
     */
    _deleteHandler = (inst) => {
        const {contact, dispatch} = inst.props;

        deleteContact(contact, dispatch);
    };

    /**
     * Handles the Edit reequest
     *
     * @param {object} inst - References the react component
     * @private
     */
    _editHandler = (inst) => {
        const {contact, dispatch} = inst.props;

        dispatch(goToContactEdit(contact));
    };

    /**
     * Toggles the delete confirmation of the contact
     *
     * @param {object} inst - Reference to the react component
     * @private
     */
    _toggleDeleteConfirm = (inst) => {
        const {confirmDelete} = inst.state;

        inst.setState({
            confirmDelete: !confirmDelete
        });
    };

    /**
     * Toggles the detail view of the contact
     *
     * @param {object} inst - Reference to the react component
     * @private
     */
    _toggleDetails = (inst) => {
        const {showDetails} = inst.state;

        inst.setState({
            showDetails: !showDetails
        });
    };

    /**********************************
     * Components
     *********************************/

    const ContactDetails = ContactDetailsFactory();
    const ContactItemActions = ContactItemActionsFactory();
    const ContactDeleteConfirm = ContactDeleteConfirmFactory();

    /**************************************************************************
     *
     * Public Interface / React Component
     *
     *************************************************************************/

    let component = createClass({
        /**
         * Used in debug messages
         */
        displayName: 'ContactListItem',
        /**
         * Specifies what we expect in 'this.props'. Allows you to validate
         * props being passed to your components.
         */
        propTypes: {
            dispatch: PropTypes.func.isRequired,
            contact: PropTypes.object.isRequired
        },
        /**
         * Invoked once before the component is mounted. The return value
         * will be used as the initial value of this.state.
         *
         * @returns {object}
         */
        getInitialState() {
            return {
                showDetails: false,
                confirmDelete: false
            };
        },
        /**
         * Invoked before rendering when new props or state are being received.
         * Use this as an opportunity to return false when you're certain
         * that the transition to the new props and state will not require
         * a component update.
         *
         * @param nextProps
         * @param nextState
         */
        shouldComponentUpdate(nextProps, nextState) {
            if (!is(this.state, nextState)) {
                return true;
            }

            if (!is(this.props.contact, nextProps.contact)) {
                return true;
            }

            return false;
        },
        /**
         * The render() method is required. Generates the virtual DOM/HTML.
         * @returns {*}
         */
        render() {
            const {contact} = this.props;
            const {confirmDelete, showDetails} = this.state;

            return <div className="contact-list-item">
                <div className='row'>
                    <div className='col-md-8'>
                        {contact.get('firstName')} {contact.get('lastName')}
                    </div>
                    {(confirmDelete) ?
                        <ContactDeleteConfirm
                            onConfirm={() => {
                                _deleteHandler(this);
                            }}
                            onCancel={() => {
                                _toggleDeleteConfirm(this);
                            }}
                        /> :
                        <ContactItemActions
                            onToggle={() => {
                                _toggleDetails(this);
                            }}
                            onEdit={() => {
                                _editHandler(this);
                            }}
                            onDelete={() => {
                                _toggleDeleteConfirm(this);
                            }}
                        />
                    }

                </div>
                {(showDetails) ? <ContactDetails contact={contact}/> : null}
            </div>;
        }
    });

    // Connects the component to the store and the dispatcher
    // In this case we don't care about store data, but we still want the dispatcher
    return connect()(component);
}

module.exports = ContactListItemFactory;