/******************************************************************************
 *
 * Imports
 *
 *****************************************************************************/

// NPM
const React = require('react');
const {createClass, PropTypes} = React;
const {is} = require('immutable');

/**
 * Generates a ContactDeleteConfirm Component
 *
 * @param {object} spec - Container for named parameters to modify private member behavior
 * @returns {*} - React Component
 * @constructor
 */
function ContactDeleteConfirmFactory(spec) {
    /**************************************************************************
     *
     * Private Members
     *
     *************************************************************************/

    /**********************************
     * Methods
     *********************************/

    /**********************************
     * Components
     *********************************/

    /**************************************************************************
     *
     * Public Interface / React Component
     *
     *************************************************************************/

    let component = createClass({
        /**
         * Used in debug messages
         */
        displayName: 'ContactDeleteConfirm',
        /**
         * Specifies what we expect in 'this.props'. Allows you to validate
         * props being passed to your components.
         */
        propTypes: {
            onConfirm: PropTypes.func.isRequired,
            onCancel: PropTypes.func.isRequired
        },
        /**
         * Invoked before rendering when new props or state are being received.
         * Use this as an opportunity to return false when you're certain
         * that the transition to the new props and state will not require
         * a component update.
         *
         * @param nextProps
         * @param nextState
         */
        shouldComponentUpdate(nextProps, nextState) {
            if (!is(this.props, nextProps)) {
                return true;
            }

            return false;
        },
        /**
         * The render() method is required. Generates the virtual DOM/HTML.
         * @returns {*}
         */
        render() {
            const {onConfirm, onCancel} = this.props;

            return <div>
                <div className='col-md-2'>
                    Are you sure?
                </div>
                <div className='col-md-1'>
                    <button className="btn btn-danger" onClick={onConfirm}>Yes</button>
                </div>
                <div className='col-md-1'>
                    <button className="btn btn-default" onClick={onCancel}>No</button>
                </div>
            </div>;
        }
    });

    return component;
}

module.exports = ContactDeleteConfirmFactory;