/******************************************************************************
 *
 * Imports
 *
 *****************************************************************************/

// NPM
const React = require('react');
const {createClass, PropTypes} = React;
const {connect} = require('react-redux');
const {is} = require('immutable');

/**
 * Generates a ContactDetails Component
 *
 * @param {object} spec - Container for named parameters to modify private member behavior
 * @returns {*} - React Component
 * @constructor
 */
function ContactDetailsFactory(spec) {
    /**************************************************************************
     *
     * Private Members
     *
     *************************************************************************/

    /**********************************
     * Methods
     *********************************/

    /**********************************
     * Components
     *********************************/

    /**************************************************************************
     *
     * Public Interface / React Component
     *
     *************************************************************************/

    let component = createClass({
        /**
         * Used in debug messages
         */
        displayName: 'ContactDetails',
        /**
         * Specifies what we expect in 'this.props'. Allows you to validate
         * props being passed to your components.
         */
        propTypes: {
            contact: PropTypes.object.isRequired
        },
        /**
         * Invoked before rendering when new props or state are being received.
         * Use this as an opportunity to return false when you're certain
         * that the transition to the new props and state will not require
         * a component update.
         *
         * @param nextProps
         * @param nextState
         */
        shouldComponentUpdate(nextProps, nextState) {
            if (!is(this.props.contact, nextProps.contact)) {
                return true;
            }

            return false;
        },
        /**
         * The render() method is required. Generates the virtual DOM/HTML.
         * @returns {*}
         */
        render() {
            const {contact} = this.props;

            return <div className="contact-details">
                <div>{contact.get('workPhone')}</div>
                <div>{contact.get('homePhone')}</div>
                <div>{contact.get('mobilePhone')}</div>
                <div>{contact.get('address').get('street')}</div>
                <div>{contact.get('address').get('street2')}</div>
                <div>{contact.get('address').get('city')}</div>
                <div>{contact.get('address').get('state')}</div>
                <div>{contact.get('address').get('zip')}</div>
            </div>;
        }
    });

    return component;
}

module.exports = ContactDetailsFactory;