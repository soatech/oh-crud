/******************************************************************************
 * 
 * Imports
 * 
 *****************************************************************************/
 
// NPM
const React = require('react');
const {createClass, PropTypes} = React;

/**
 * Generates a Header Component
 * 
 * @param {object} spec - Container for named parameters to modify private member behavior
 * @returns {*} - React Component
 * @constructor
 */
function HeaderFactory(spec) {
    /**************************************************************************
     * 
     * Private Members
     *
     *************************************************************************/
     
    /**************************************************************************
     * 
     * Public Interface / React Component
     *
     *************************************************************************/
     
    return createClass({
        /**
         * Used in debug messages
         */
        displayName: 'Header',
        /**
         * The render() method is required. Generates the virtual DOM/HTML.
         * @returns {*}
         */
        render() {
            return <div className="header">
                <span>Manage My Contacts</span>
            </div>;
        }
    });
}

module.exports = HeaderFactory;