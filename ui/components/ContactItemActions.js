/******************************************************************************
 *
 * Imports
 *
 *****************************************************************************/

// NPM
const React = require('react');
const {createClass, PropTypes} = React;
const {is} = require('immutable');

/**
 * Generates a ContactItemActions Component
 *
 * @param {object} spec - Container for named parameters to modify private member behavior
 * @returns {*} - React Component
 * @constructor
 */
function ContactItemActionsFactory(spec) {
    /**************************************************************************
     *
     * Private Members
     *
     *************************************************************************/

    /**********************************
     * Methods
     *********************************/

    /**********************************
     * Components
     *********************************/

    /**************************************************************************
     *
     * Public Interface / React Component
     *
     *************************************************************************/

    let component = createClass({
        /**
         * Used in debug messages
         */
        displayName: 'ContactItemActions',
        /**
         * Specifies what we expect in 'this.props'. Allows you to validate
         * props being passed to your components.
         */
        propTypes: {
            onToggle: PropTypes.func.isRequired,
            onEdit: PropTypes.func.isRequired,
            onDelete: PropTypes.func.isRequired
        },
        /**
         * Invoked before rendering when new props or state are being received.
         * Use this as an opportunity to return false when you're certain
         * that the transition to the new props and state will not require
         * a component update.
         *
         * @param nextProps
         * @param nextState
         */
        shouldComponentUpdate(nextProps, nextState) {
            if (!is(this.props, nextProps)) {
                return true;
            }

            return false;
        },
        /**
         * The render() method is required. Generates the virtual DOM/HTML.
         * @returns {*}
         */
        render() {
            const {onToggle, onEdit, onDelete} = this.props;

            return <div>
                <div className='col-md-1'>
                    <button className="btn btn-default" onClick={onToggle}>Details</button>
                </div>
                <div className='col-md-1'>
                    <button className="btn btn-primary" onClick={onEdit}>Edit</button>
                </div>
                <div className='col-md-1'>
                    <button className="btn btn-danger" onClick={onDelete}>Delete</button>
                </div>
            </div>;
        }
    });

    return component;
}

module.exports = ContactItemActionsFactory;