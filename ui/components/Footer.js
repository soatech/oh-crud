/******************************************************************************
 * 
 * Imports
 * 
 *****************************************************************************/
 
// NPM
const React = require('react');
const {createClass, PropTypes} = React;

/**
 * Generates a Footer Component
 * 
 * @param {object} spec - Container for named parameters to modify private member behavior
 * @returns {*} - React Component
 * @constructor
 */
function FooterFactory(spec) {
    /**************************************************************************
     * 
     * Private Members
     *
     *************************************************************************/
     
    /**************************************************************************
     * 
     * Public Interface / React Component
     *
     *************************************************************************/
     
    return createClass({
        /**
         * Used in debug messages
         */
        displayName: 'Footer',
        /**
         * The render() method is required. Generates the virtual DOM/HTML.
         * @returns {*}
         */
        render() {
            return <div className="footer">
                <div>All Rights Reserved, Callihan!</div>
                <div>What about the rights of that little girl?!</div>
            </div>;
        }
    });
}

module.exports = FooterFactory;