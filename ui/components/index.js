module.exports = {
    ContactListItemFactory: require('./ContactListItem'),
    FooterFactory: require('./Footer'),
    HeaderFactory: require('./Header')
};