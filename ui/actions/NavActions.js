/******************************************************************************
 *
 * Imports
 *
 *****************************************************************************/

// Enums
const {ACTIONS, MODES, VIEWS} = require('../enums');

/**
 * Generates Actions that can be executed in relation to Nav
 *
 * @param {object} spec - Container for named parameters to adjust private member behavior
 * @returns {object}
 * @constructor
 */
function NavActionsFactory(spec) {
    /**************************************************************************
     *
     * Private Members
     *
     *************************************************************************/

    /**********************************
     * Methods
     *********************************/

    let _goToContactAdd;
    let _goToContactEdit;
    let _goToContactList;
    let _updateNav;

    /**
     *
     * @param contact
     * @returns {object}
     * @private
     */
    _goToContactAdd = (contact) => {
        return {
            type: ACTIONS.NAV_UPDATE,
            payload: {
                view: VIEWS.CONTACT_EDIT,
                mode: MODES.ADD,
                selectedContact: contact
            }
        };
    };

    /**
     *
     * @param contact
     * @returns {object}
     * @private
     */
    _goToContactEdit = (contact) => {
        return {
            type: ACTIONS.NAV_UPDATE,
            payload: {
                view: VIEWS.CONTACT_EDIT,
                mode: MODES.EDIT,
                selectedContact: contact
            }
        };
    };

    /**
     *
     * @param contact
     * @returns {object}
     * @private
     */
    _goToContactList = () => {
        return {
            type: ACTIONS.NAV_UPDATE,
            payload: {
                view: VIEWS.CONTACT_LIST,
                mode: null,
                selectedContact: null
            }
        };
    };

    /**
     * Makes a request to the server API to add or update a contact.  On success it updates the contact in the collection.
     *
     * @param {Object} payload
     * @param {function} dispatch
     * @private
     */
    _updateNav = (payload) => {
        return {
            type: ACTIONS.NAV_UPDATE,
            payload
        };
    };

    /**************************************************************************
     *
     * Public Interface
     *
     *************************************************************************/

    return {
        updateNav: _updateNav,
        goToContactAdd: _goToContactAdd,
        goToContactEdit: _goToContactEdit,
        goToContactList: _goToContactList
    };
}

module.exports = NavActionsFactory;