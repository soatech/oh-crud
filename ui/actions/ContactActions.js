/******************************************************************************
 *
 * Imports
 *
 *****************************************************************************/

/* global fetch */

// NPM
const uuid = require('node-uuid');

// Enums
const {ACTIONS, VIEWS} = require('../enums');

/**
 * Generates Actions that can be executed in relation to Contacts
 *
 * @param {object} spec - Container for named parameters to adjust private member behavior
 * @returns {object}
 * @constructor
 */
function ContactActionsFactory(spec) {
    /**************************************************************************
     *
     * Private Members
     *
     *************************************************************************/

    /**********************************
     * Methods
     *********************************/

    let _deleteContact;
    let _loadContacts;
    let _saveContact;

    /**
     * Makes a request to the server API to delete the contact and on success, removes the contact from the app state
     *
     * @param {Record|ContactRecord} contact
     * @param {function} dispatch
     * @private
     */
    _deleteContact = (contact, dispatch) => {
        fetch(`/contacts/${contact.get('id')}`, {
            method: 'DELETE'
        }).then((response) => {
            console.log(response);

            if(!response.ok) {
                // dispatch(hideLoading());
                // dispatch(setMessage({type: 'error', text: response.message || 'There was an error creating the archives.'}));
            }

            return response.json();
        }).then((json) => {
            console.log(json);

            // dispatch(hideLoading());

            if(json.success) {
                dispatch({
                    type: ACTIONS.CONTACT_DELETE,
                    contact
                });
            } else {
                // dispatch(setMessage({type: 'error', text: json.message || 'There was an error creating the archives.'}));
            }
        }).catch((err) => {
            console.error(err);

            // error message
        });
    };

    /**
     * Makes a request to the server API to fetch all the contacts.  On success it sets this collection on the app state.
     *
     * @param {function} dispatch
     * @private
     */
    _loadContacts = (dispatch) => {
        fetch('/contacts')
            .then((response) => {
                if(!response.ok) {
                    // error messages
                    console.error(response);
                }

                return response.json();
            })
            .then((json) => {
                if(json.success) {
                    dispatch({
                        type: ACTIONS.CONTACTS_LOAD,
                        contacts: json.contacts
                    });
                } else {
                    // error message
                    console.error(json.message);
                }
            })
            .catch((err) => {
                console.error(err);

                // error message
            });
    };

    /**
     * Makes a request to the server API to add or update a contact.  On success it updates the contact in the collection.
     *
     * @param {Object} contact
     * @param {function} dispatch
     * @private
     */
    _saveContact = (contact, dispatch) => {
        let type = (!contact.id) ? ACTIONS.CONTACT_ADD : ACTIONS.CONTACT_UPDATE;
        let url = '/contacts';
        let method = 'POST';
        
        if(type === ACTIONS.CONTACT_UPDATE) {
            url = `/contacts/${contact.id}`;
            method = 'PUT';
        }

        fetch(url, {
            method: method,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(contact)
        }).then((response) => {
            console.log(response);

            if(!response.ok) {
                // dispatch(hideLoading());
                // dispatch(setMessage({type: 'error', text: response.message || 'There was an error creating the archives.'}));
            }

            return response.json();
        }).then((json) => {
            console.log(json);

            // dispatch(hideLoading());

            if(json.success) {
                dispatch({
                    type,
                    contact: json.contact
                });

                dispatch({
                    type: ACTIONS.NAV_UPDATE,
                    payload: {
                        view: VIEWS.CONTACT_LIST,
                        selectedContact: null,
                        mode: null
                    }
                });
            } else {
                // dispatch(setMessage({type: 'error', text: json.message || 'There was an error creating the archives.'}));
            }
        }).catch((err) => {
            console.error(err);

            // error message
        });
    };

    /**************************************************************************
     *
     * Public Interface
     *
     *************************************************************************/

    return {
        deleteContact: _deleteContact,
        loadContacts: _loadContacts,
        saveContact: _saveContact
    };
}

module.exports = ContactActionsFactory;