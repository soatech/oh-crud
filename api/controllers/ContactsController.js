/******************************************************************************
 *
 * Imports
 *
 *****************************************************************************/

// NPM
const mongoose = require('mongoose');

/******************************************************************************
 *
 * Private Members
 *
 *****************************************************************************/

/**************************************
 * Variables
 *************************************/

const Contact = mongoose.model('Contact');

/**************************************
 * Methods
 *************************************/

let _createContact;
let _loadContacts;
let _loadByContactId;
let _removeContact;
let _updateContact;

/**
 * Adds a Contact to the collection
 *
 * @param req
 * @param res
 * @param next
 */
_createContact = (req, res, next) => {
    let contact = new Contact(req.body);

    contact.save((err) => {
        if(err) {
            return next(err);
        } else {
            res.json({success: true, contact});
        }
    });
};

/**
 * Sends the current Contacts collection
 *
 * @param req
 * @param res
 * @param next
 */
_loadContacts = (req, res, next) => {
    Contact.find((err, contacts) => {
        if(err) {
            console.log(err);
            next(err);
        } else {
            res.json({success: true, contacts});
        }
    });
};

/**
 * Maps the url parameter and adds it to the request
 * 
 * @param req
 * @param res
 * @param next
 * @param id
 */
_loadByContactId = (req, res, next, id) => {
    Contact.findOne({
        _id: id
    }, (err, contact) => {
        if(err) {
            return next(err);
        } else {
            req.contact = contact;
            next();
        }
    });
};

/**
 * Removes a Contact from the collection
 *
 * @param req
 * @param res
 * @param next
 */
_removeContact = (req, res, next) => {
    req.contact.remove((err) => {
        if(err) {
            return next(err);
        } else {
            res.json({success: true});
        }
    });
};

/**
 * Updates a Contact on the collection
 *
 * @param req
 * @param res
 * @param next
 */
_updateContact = (req, res, next) => {
    Contact.findByIdAndUpdate(req.contact.id, req.body, {new: true}, (err, contact) => {
        if(err) {
            return next(err);
        } else {
            res.json({success: true, contact});
        }
    });
};

/******************************************************************************
 *
 * Public Interface
 *
 *****************************************************************************/
 
module.exports = {
    create: _createContact,
    load: _loadContacts,
    loadByContactId: _loadByContactId,
    remove: _removeContact,
    update: _updateContact
};