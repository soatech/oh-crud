

exports.index = (req, res, next) => {
    let mode = process.env.NODE_ENV || 'development';
    let script = 'main.js';
    let css = 'main.css';

    if(mode === 'production') {
        script = 'main.min.js';
        css = 'main.min.css';
    }

    res.render('index', {
        title: 'Oh CRUD! - A Contact Manager',
        script: script,
        css: css
    });
};