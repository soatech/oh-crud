var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/*
firstName: 'Sally',
    lastName: 'Sallerson',
    workPhone: '123-456-7890',
    homePhone: '234-567-8901',
    mobilePhone: '345-678-9012',
    address: {
        street: '123 Anywhere St.',
        street2: undefined,
        city: 'The City',
        state: 'NY',
        zip: '12345'
    }
    */

var AddressSchema = new Schema({
    street: {
        type: String
    },
    street2: {
        type: String
    },
    city: {
        type: String
    },
    state: {
        type: String
    },
    zip: {
        type: Number
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

AddressSchema.pre('save', function(next) {
    this.updated = new Date();

    next();
});

mongoose.model('Address', AddressSchema);