var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ContactSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    workPhone: {
        type: String
    },
    homePhone: {
        type: String
    },
    mobilePhone: {
        type: String
    },
    address: {
        type: Schema.ObjectId,
        ref: 'Address'
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

ContactSchema.pre('save', function(next) {
    this.updated = new Date();

    next();
});

mongoose.model('Contact', ContactSchema);