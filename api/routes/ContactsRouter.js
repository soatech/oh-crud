let express = require('express');
let router = express.Router();

let {create, load, loadByContactId, remove, update} = require('../controllers/ContactsController');

router.get('/', load);

router.post('/', create);

router.put('/:contactId', update)

router.delete('/:contactId', remove);

router.param('contactId', loadByContactId);

module.exports = router;