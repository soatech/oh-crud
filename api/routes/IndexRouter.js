let express = require('express');
let router = express.Router();

let {index} = require('../controllers/IndexController');

/* GET home page. */
router.get('/', index);

module.exports = router;
