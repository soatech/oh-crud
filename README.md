# Overview

An example CRUD application via MERN (MongoDB, ExpressJS, ReactJS, NodeJS).

# Technologies Used

### Server API

 * [Node v.6.3.0](https://nodejs.org)
 * [ExpressJS](https://expressjs.com/)
 * [MongoDB](https://mongodb.com)
 * [EJS](https://www.npmjs.com/package/ejs)
 * [Mongoose](http://mongoosejs.com)

### UI

 * [ReactJS](https://facebook.github.io/react/)
 * [LESS](http://lesscss.org/)
 * [Bootstrap](http://getbootstrap.com)
 * [Zeker](https://www.npmjs.com/package/zeker)
 * [Redux](http://redux.js.org)
 * [ImmutableJS](https://facebook.github.io/immutable-js/)
 * [React Loose Forms](https://www.npmjs.com/search?q=react-loose-forms)

# Setup

Before you can prepare and run the app you need to have NodeJS and MongoDB 
installed.  Both of these can be installed via `apt` but will have outdated 
versions.  In order to get latest versions you will need to download or follow
installation instructions from their websites.

### NodeJS

There are a few options for NodeJS.  First is to go to their [website](https://nodejs.org/) 
and manually download and compile/install the latest 6.x version.

The more robust approach is to use [nvm](https://github.com/creationix/nvm) and install
the different versions you might need for different projects.

### MongoDB

In order to get an up to date version you will need to follow installation instructions
on their website.

 * [Ubuntu](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)
 * [Debian](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-debian/)
 * [OSX](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/)
 * [Windows](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/)

Most installations will setup MongoDB to be run as a service.  So you just need
to run `sudo service start mongodb`.  If this did not happen (for example, on c9.io)
then you can manually set it up and get it running.

`$ mkdir data`
`$ echo 'mongod --bind_ip=$IP --dbpath=data --nojournal --rest "$@"' > mongod`
`$ chmod a+x mongod`
`$ ./mongod`

### NMP

With Node installed you will have npm installed as well.  You just need to instruct
it to install the local packages for the app.

`$ npm install`

# Running

You can run the API and the UI separately if you wish, but the best way is to just 
run them together.

`$ node ./bin/www`

This is also run with the shortcut `npm start`

# Development

If you are editing the code you will want to compiler running.

`npm run watch`

See what other scripts are available to run by entering `npm run` with no additional
parameters.
 
 